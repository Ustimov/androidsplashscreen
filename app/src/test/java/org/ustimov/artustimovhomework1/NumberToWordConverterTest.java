package org.ustimov.artustimovhomework1;

import junit.framework.Assert;

import org.junit.Test;

import static org.junit.Assert.*;

public class NumberToWordConverterTest {

    @Test
    public void testConvert() throws Exception {
        Assert.assertEquals("один", NumberToWordConverter.Convert(1));
        Assert.assertEquals("десять", NumberToWordConverter.Convert(10));
        Assert.assertEquals("пятнадцать", NumberToWordConverter.Convert(15));
        Assert.assertEquals("двадцать пять", NumberToWordConverter.Convert(25));
        Assert.assertEquals("пятьдесят", NumberToWordConverter.Convert(50));
        Assert.assertEquals("сто", NumberToWordConverter.Convert(100));
        Assert.assertEquals("тристо сорок", NumberToWordConverter.Convert(340));
        Assert.assertEquals("семьсот шестьдесят один", NumberToWordConverter.Convert(761));
        Assert.assertEquals("тысяча", NumberToWordConverter.Convert(1000));
    }
}
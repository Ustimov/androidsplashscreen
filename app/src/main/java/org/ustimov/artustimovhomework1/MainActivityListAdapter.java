package org.ustimov.artustimovhomework1;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MainActivityListAdapter extends BaseAdapter {

    private Activity context;

    public MainActivityListAdapter(Activity context) {
        super();
        this.context = context;
    }

    @Override
    public int getCount() {
        return 1000;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = context.getLayoutInflater().inflate(R.layout.list_view_item, null);
        }

        TextView textView = (TextView)view.findViewById(R.id.label);
        textView.setText(NumberToWordConverter.Convert(position + 1));

        if (position % 2 == 1) {
            view.setBackgroundColor(Color.GRAY);
        } else {
            view.setBackgroundColor(Color.WHITE);
        }

        return view;
    }
}

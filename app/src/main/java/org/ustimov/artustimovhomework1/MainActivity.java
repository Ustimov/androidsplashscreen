package org.ustimov.artustimovhomework1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);

        ListView listview = (ListView) findViewById(R.id.list_view);

        MainActivityListAdapter adapter = new MainActivityListAdapter(this);

        listview.setAdapter(adapter);
    }
}

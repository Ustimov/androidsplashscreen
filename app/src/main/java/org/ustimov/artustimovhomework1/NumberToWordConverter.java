package org.ustimov.artustimovhomework1;

public class NumberToWordConverter {
    public static String Convert(int number) {
        if (number == 1000) {
            return "тысяча";
        }

        String input = Integer.toString(number);

        StringBuilder result = new StringBuilder();

        input = result.append(input).reverse().toString();

        result.setLength(0);

        for (int i = input.length() - 1; i >= 0; i--) {
            if (i == 0) {
                if (input.length() != 1 && input.charAt(i) != '0') {
                    result.append(" ");
                }
                result.append(getUnits(input.charAt(i)));
            } else if (i == 1) {
                if (input.length() != 2 && input.charAt(i) != '0') {
                    result.append(" ");
                }
                if (input.charAt(i) == '1' && input.charAt(i - 1) != '0') {
                    result.append(getSecondTen(input.charAt(i - 1)));
                    break;
                } else {
                    result.append(getTens(input.charAt(i)));
                }
            } else {
                result.append(getHundreds(input.charAt(i)));
            }
        }
        return result.toString();
    }

    private static String getUnits(char c) {
        switch (c) {
            case '1': return "один";
            case '2': return "два";
            case '3': return "три";
            case '4': return "четыре";
            case '5': return "пять";
            case '6': return "шесть";
            case '7': return "семь";
            case '8': return "восемь";
            case '9': return "девять";
        }
        return "";
    }

    private static String getTens(char c) {
        switch (c) {
            case '1': return "десять";
            case '2': return "двадцать";
            case '3': return "тридцать";
            case '4': return "сорок";
            case '5': return "пятьдесят";
            case '6': return "шестьдесят";
            case '7': return "семьдесят";
            case '8': return "восемьдесят";
            case '9': return "девяносто";
        }
        return "";
    }

    private static String getSecondTen(char c) {
        switch (c) {
            case '1': return "одинадцать";
            case '2': return "двенадцать";
            case '3': return "тринадцать";
            case '4': return "четырнадцать";
            case '5': return "пятнадцать";
            case '6': return "шестнадцать";
            case '7': return "семнадцать";
            case '8': return "восемнадцать";
            case '9': return "девятнадцать";
        }
        return "";
    }

    private static String getHundreds(char c) {
        switch (c) {
            case '1': return "сто";
            case '2': return "двести";
            case '3': return "тристо";
            case '4': return "четыресто";
            case '5': return "пятьсот";
            case '6': return "шестьсот";
            case '7': return "семьсот";
            case '8': return "восемьсот";
            case '9': return "девятьсот";
        }
        return "";
    }
}
